
# README #

Erick Renteria QA Homework
### What is this repository for? ###

Homework for Onprem

### How do I get set up? ###

IDE: Pycharm. 
Modules needed: Selenium webdriver, unittest, HTMLTestRunner.
Add the path to gekopdriver for Firefox in line 22(included in the files for MAC OS).
HTMLTestRunner is also included. 


### Running code ###
You can run the code by command line. 
Navigate to dir and run: python unit_test.py > testResults.html
Results can be found under the testResults.html file.

### Questions ###

Please contact me with any question you might have. 
Erick Renteria 312.371.0222.
erick.renteria1@gmail.com