#Erick Renteria Home work Task for OnPrem Solutions Partners LLC

"""This script will navigate the Firefox browser to OnPren website
and click on the Team button. Once in the Team page the scrip will do a
search for partner and will account the total number of partners.

"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
import HTMLTestRunner
import unittest

class UnitTest(unittest.TestCase):

    #Function for our setup
    def setUp(self):
        self.driver = webdriver.Firefox(executable_path=r'/Users/erenteria/HomeWork/qa-homework/geckodriver')
        self.base_url = "http://www.onprem.com"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_unit(self):
        driver = self.driver
        #regelem = '//*[@id="myInput"]'
        """I'm checking for Aaron's xpath before we move on
        so we can fins the next element correctly"""
        Aaron = '//*[@id="staff-list"]/div[1]/div/a/div/img'

        driver.get(self.base_url + "/index.html")
        #Lokking for the Team Button with id.
        firstele = driver.find_element_by_id("team")
        #the click() option did not work here so, I had to use Enter to click on the button.
        firstele.send_keys(Keys.ENTER)
        #Wait until we see Aaron profile to validate data upload has been completed.
        Wait(driver, 10).until(EC.presence_of_element_located((By.XPATH, Aaron)))
        #I clear the search box no make sure my input is correct.
        driver.find_element_by_xpath('//*[@id="myInput"]').clear()
        #Looking for the search filed and input partner.
        driver.find_element_by_xpath('//*[@id="myInput"]').send_keys("partner" + Keys.RETURN)

        #checking that Christophe Ponsart is with this page of partners with assert.
        try:
            assert driver.find_element_by_xpath('/html/body/div[2]/section[2]/div/div/div[1]/div[20]/div')
            print ('All good')
        except Exception as e:
            print ('Something is wrong')

        #We look for the style that are present with the xpath and count them.
        print len(driver.find_elements_by_xpath("//div[@style='display: inline-block;']"))


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    HTMLTestRunner.main()
